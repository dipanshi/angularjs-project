(function(){
  var app = angular.module('StudentDetails',['ngRoute','ngStorage']);

  app.config(['$routeProvider','$locationProvider',function($routeProvider,$locationProvider){
    $routeProvider
      .when('/index',
        {
          controller: 'StudentDetailsController',
          templateUrl: './Partials/home.html'
        })
      .when('/form',
        {
          controller: 'SaveDataController',
          templateUrl: './Partials/form.html'
        })
      .when('/display',
        {
          controller: 'GetDataController',
          templateUrl: './Partials/display.html'
        })
      .when('/detailDisplay',
        {
          controller: 'StudentDetailsController',
          templateUrl: './Partials/detail.html'
        })
      .otherwise({ redirectTo: '/index' });
  }]);


  app.controller('SaveDataController', ['$scope','$localStorage',
    function($scope,$localStorage){
      console.log("SaveDataController============");
      $localStorage.studentsData = $localStorage.studentsData || [];
      $scope.Save = function () {
        var student = {};
        student.fname = $scope.fname;
        student.lname = $scope.lname;
        student.dob = $scope.dob;
        student.email = $scope.email;
        student.number = $scope.number;

        $localStorage.studentsData.push(student);
        console.log($localStorage.studentsData);
      }
  }]);

  app.controller('GetDataController', ['$scope','$localStorage',
    function($scope,$localStorage){
      console.log("GetDataController============");
      console.log($localStorage.studentsData);
      $scope.fname;
      $scope.lname;
      $scope.dob;
      $scope.email;
      $scope.number;

      $scope.studentsData = $localStorage.studentsData;

    }]);

  app.controller('StudentDetailsController',  function($scope,$localStorage){

  });

  app.controller('FormDataController', ['$scope','$http',
    function($scope,$http){
      console.log("FormDataController============");
      $http.get('form.json')
        .then(function(response){
          $scope.fieldset = response.data;
        });

    }]);

  app.service('FormDataSevice',function () {

  });

})();